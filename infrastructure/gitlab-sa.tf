resource "kubernetes_service_account" "gitlab" {
  metadata {
    name = "gitlab"
    namespace = "kube-system"
  }
  automount_service_account_token = true
}

resource "kubernetes_cluster_role_binding" "gitlab" {
  metadata {
    name = "gitlab-crb"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    name  = "cluster-admin"
    kind  = "ClusterRole"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "gitlab"
    namespace = "kube-system"
  }
}

data "kubernetes_secret" "gitlab" {
  metadata {
    name = kubernetes_service_account.gitlab.default_secret_name
    namespace = "kube-system"
  }
}