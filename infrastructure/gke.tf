resource "random_pet" "main" {
  length = 3
  prefix = "ls"
}

resource "google_container_cluster" "main" {
  name               = random_pet.main.id
  location           = "europe-west4" # NL
  initial_node_count = 1

  master_auth {
    client_certificate_config {
      issue_client_certificate = true
    }
  }
}
