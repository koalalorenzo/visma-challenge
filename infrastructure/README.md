## Setup GitLab Credentials for Terraform

Create a Personal Token ([here](https://gitlab.com/profile/personal_access_tokens))
that has the permission to use the API and read/write the registries.

The [documentation](https://gitlab.com/gitlab-org/terraform-images) by defaults 
uses `CI_BUILD_TOKEN` and `GITLAB_USER_LOGIN` but it seems to fail. So this is a
short workaround, but the code is ready to work when that will be updated.

Then you can run the following CLI command to set the variable:

```bash
GITLAB_TOKEN= # The token just created
GITLAB_USERNAME= # Your username
GITLAB_PROJECT_ID=21776020 # Available from the Project Settings page
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables" \
    --form "key=GITLAB_TOKEN" \
		--form "value=$GITLAB_TOKEN"
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables" \
    --form "key=GITLAB_USERNAME" \
		--form "value=$GITLAB_USERNAME"
# Update the existing variable, in case the previous command fails
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/GITLAB_TOKEN" \
		--form "value=$GITLAB_TOKEN"
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/GITLAB_USERNAME" \
		--form "value=$GITLAB_USERNAME"
```

## Setup GitLab Credentials for GCP

Create a Service Account:

```shell
gcloud iam service-accounts create gitlab \
    --description="gitlab integration for terraform" \
    --display-name="gitlab"
```

Grant the permissions to the service account:

```shell
gcloud projects add-iam-policy-binding snbx-econo2-cm \
  --member serviceAccount:gitlab@snbx-econo2-cm.iam.gserviceaccount.com \
  --role roles/admin
```

Generate a new Key:

```shell
gcloud iam service-accounts keys create ~/gitlab@snbx-econo2-cm.json \
  --iam-account gitlab@snbx-econo2-cm.iam.gserviceaccount.com
```

Create GitLab project Variables:

```shell
GITLAB_TOKEN= # Get it from your user account
GITLAB_PROJECT_ID=21776020
# Create the Variable if not existing
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables" \
    --form "key=GOOGLE_CLOUD_KEYFILE_JSON" \
		--form "value=`cat ~/gitlab@snbx-econo2-cm.json`"
# Update the existing variable, in case the previous command fails
curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
		"https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/variables/GOOGLE_CLOUD_KEYFILE_JSON" \
		--form "value=`cat ~/gitlab@snbx-econo2-cm.json`"
```

