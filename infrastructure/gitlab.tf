data "google_client_config" "main" {}

data "gitlab_project" "main" {
  id = var.gitlab_project_id
}

# Configuring it directly, not using Vault to generate dynamic secrets here
resource "gitlab_project_cluster" "main" {
  project            = data.gitlab_project.main.id
  name               = "main"
  enabled            = true
  kubernetes_api_url = "https://${google_container_cluster.main.endpoint}"
  kubernetes_token   = lookup(data.kubernetes_secret.gitlab.data, "token")
  kubernetes_ca_cert = base64decode(google_container_cluster.main.master_auth.0.cluster_ca_certificate)
  environment_scope  = "*"
}

# This token will generate and allow us to deploy in k8s, injecting it in the 
# pipelien automagically. 😍
resource "gitlab_deploy_token" "deploy_token" {
  project = data.gitlab_project.main.id
  name    = "gitlab-deploy-token"

  scopes = ["read_registry"]

  lifecycle {
    ignore_changes = [
      expires_at
    ]
  }
}