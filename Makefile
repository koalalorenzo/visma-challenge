# Setting default values if not running in GitLab Pipeline (ex: locally)
CI_REGISTRY ?= registry.gitlab.com
CI_DEPLOY_USER ?= ${GITLAB_USERNAME}
CI_DEPLOY_PASSWORD ?= ${GITLAB_TOKEN}

.DEFAULT_GOAL := default

build_%: 
	$(MAKE) -C $* docker_build

push_%: 
	$(MAKE) -C $* docker_push

start_%:
	$(MAKE) -C charts deploy_$* -e HELM_EXTRA_ARGS="--wait --atomic"

test_%:
	$(MAKE) -C charts test_$*

stop_%:
	$(MAKE) -C charts uninstall_$*

deploy_token:
	-kubectl create secret docker-registry gitlab-registry \
		--docker-server=${CI_REGISTRY} \
		--docker-username=${CI_DEPLOY_USER} \
		--docker-password=${CI_DEPLOY_PASSWORD}
	kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "gitlab-registry"}]}'
.PHONY: deploy_token

build: build_dummy-pdf-or-png build_mime-proxy
start: 
	$(MAKE) start_dummy-pdf-or-png -e EXTRA_ANNOTATIONS="" 
	$(MAKE) start_mime-proxy
test: test_dummy-pdf-or-png test_mime-proxy
stop: stop_mime-proxy stop_dummy-pdf-or-png 
.PHONY: start build test stop

default: build start test 
.PHONY: default
