# Recruitment assignment for Site Reliability Engineer
This repository contains the source code for my assignment/test/challenge 
for Site Reliability Engineer. The full challenge is available at 
[CHALLENGE.md](CHALLENGE.md) 

To test the application and get the _production_ URL, visit GitLab CI/CD 
Environments page [here](https://gitlab.com/koalalorenzo/visma-challenge/-/environments)

Prometheus metrics are exported and are accessible 
[here](https://gitlab.com/koalalorenzo/visma-challenge/-/metrics)

## Requirements
To test locally the solution you need:

* Docker for Desktop (with kubernetes cluster enabled)
* GNU Make
* Helm
* kubectl

To deploy the solution infrastructure in the cloud, you also need:

* Terraform (version 0.13 or higher)
* A cup of coffee, tea, or your fav drink

## Running locally
To run this solution locally, you can simply run:

```bash
make 
```

Or, specifying all te targets:

```bash
make build start test
```

This will build the docker containers and deploy them in k8s. If all is using
the default configuration, you can test it by running:

```bash
curl http://localhost:8080/ -L -D-
```

## Deploying Infrastructure
The terraform setup is managed by the GitLab CI/CD Pipeline. The state file is 
[saved on GitLab integration](https://docs.gitlab.com/ee/user/infrastructure/) 
and it can be accessed manually but it requires in your shell environment to 
have `GITLAB_USERNAME` and `GITLAB_TOKEN` set up.

**The initial setup is partially manual**: creating a service account on GCP and 
updating the env variables in GitLab CI/CD settings. All this is automated via 
some API calls and commands. To know more check out 
[infrastructure/README.md](infrastructure/README.md).

The terraform setup will:

* Deploy a kubernetes cluster
* Create a dedicated k8s service account with ClusterAdmin role
* Set up the GitLab managed kubernetes clusters

This is required to enable `environment` deployment and have GitLab to create 
namespaces and service accounts, and populate the kueconfig files/variables when
deploying a new environment for review or production.
