package monitoring

import (
	"fmt"
	"net/http"
	"time"

	"github.com/heptiolabs/healthcheck"
	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/conf"
)

// SetupHealthChecks will create in a GoRoutine the HC kubernetes needs
func SetupHealthChecks(c *conf.Configuration) error {
	health := healthcheck.NewHandler()

	// Just make sure that we are alive
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(1000))

	// Our app is not ready if we can't reach the URL within 500 milliseconds
	health.AddReadinessCheck("upstream-check", healthcheck.HTTPGetCheck(c.GetUpstreamUrl(), 500*time.Millisecond))

	return http.ListenAndServe(fmt.Sprintf("0.0.0.0%s", c.HealthAddrListen), health)
}
