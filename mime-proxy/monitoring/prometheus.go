package monitoring

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/conf"
)

// SetupPrometheusEndpoint will expose the prometheus http server
func SetupPrometheusEndpoint(c *conf.Configuration) error {
	http.Handle("/metrics", promhttp.Handler())
	return http.ListenAndServe(fmt.Sprintf("0.0.0.0%s", c.PrometheusAddrListen), nil)
}
