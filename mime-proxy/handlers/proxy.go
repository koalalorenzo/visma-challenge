package handlers

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httputil"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	proxy *httputil.ReverseProxy

	typeCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "mimeproxy_type",
		Help: "The total number of content provided by type",
	}, []string{"type"})
)

func init() {
	prometheus.MustRegister(typeCounter)
}

func responseModifier(r *http.Response) error {
	// Copy the response so that the body is not cloned

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	ct := http.DetectContentType(body[:513])
	r.Header.Set("Content-Type", ct)

	r.Body = ioutil.NopCloser(bytes.NewReader(body))
	typeCounter.With(prometheus.Labels{"type": ct}).Inc()
	return nil
}

func getDataByID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	_ = vars["id"]
	proxy.ServeHTTP(w, r)
}
