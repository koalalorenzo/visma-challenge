package handlers

import (
	"fmt"
	"math/rand"
	"net/http"
)

func redirectToRandomNumber(w http.ResponseWriter, r *http.Request) {
	num := rand.Int()
	http.Redirect(w, r, fmt.Sprintf("/%d", num), http.StatusTemporaryRedirect)
}
