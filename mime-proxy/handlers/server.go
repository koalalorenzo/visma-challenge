package handlers

import (
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/gorilla/mux"
	negronilogrus "github.com/meatballhat/negroni-logrus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"github.com/urfave/negroni"

	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/conf"
)

var (
	httpCounter = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "mimeproxy_http",
		Help: "Number of HTTP requests by status code & method",
	}, []string{"code", "method"})
)

var (
	appConf *conf.Configuration
	log     *logrus.Logger
)

// Listen will set up and start and listens for HTTP requests.
// It accepts only the configuration that defines the port/address to listen to.
func Listen(c *conf.Configuration, l *logrus.Logger) error {
	appConf = c
	log = l

	target, _ := url.Parse(c.GetUpstreamUrl())
	proxy = httputil.NewSingleHostReverseProxy(target)
	proxy.ModifyResponse = responseModifier

	// Set up Routing
	rmux := mux.NewRouter()
	rmux.Path("/").HandlerFunc(redirectToRandomNumber)
	rmux.Path("/{id}").HandlerFunc(getDataByID)

	// Adding some drinks to make it easier 🍸
	n := negroni.New()
	n.Use(negronilogrus.NewCustomMiddleware(logrus.InfoLevel, &logrus.TextFormatter{}, "api"))
	n.UseHandler(rmux)

	log.Infof("Listening to HTTP request at %s", c.HTTPAddrListen)
	return http.ListenAndServe(c.HTTPAddrListen, promhttp.InstrumentHandlerCounter(httpCounter, n))
}
