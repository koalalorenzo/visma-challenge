package conf

/*
 * In this file you will find the code related to checking the environment for
 * configuration
 */

import (
	"encoding/json"
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

// Configuration of the app
type Configuration struct {
	HTTPAddrListen       string `split_words:"true" default:":8080"`
	HealthAddrListen     string `split_words:"true" default:":8086"`
	PrometheusAddrListen string `split_words:"true" default:":2112"`

	Log struct {
		Format string `split_words:"true" default:"text"`
		Level  string `split_words:"true" default:"debug"`
	}

	Upstream struct {
		URL         string `split_words:"false"`
		ServicePort string `envconfig:"DUMMY_PDF_OR_PNG_SERVICE_PORT_HTTP" default:"3000"`
		ServiceHost string `envconfig:"DUMMY_PDF_OR_PNG_SERVICE_HOST"`
	}
}

// ParseConfiguration will check ENV variables and populate Configuration
func ParseConfiguration() (*Configuration, error) {
	var c Configuration
	err := envconfig.Process("", &c)
	return &c, err
}

// Usage will show to console the usage
func (c *Configuration) Usage() {
	envconfig.Usage("", c)
}

// ToJSON the current configuration
func (c *Configuration) ToJSON() string {
	out, _ := json.MarshalIndent(c, "", "\t")
	return fmt.Sprintf("%s", out)
}

func (c *Configuration) GetUpstreamUrl() string {
	if len(c.Upstream.ServiceHost) > 0 {
		return fmt.Sprintf("http://%s:%s", c.Upstream.ServiceHost, c.Upstream.ServicePort)
	}
	return c.Upstream.URL
}
