module gitlab.com/koalalorenzo/visma-challenge/mime-proxy

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/heptiolabs/healthcheck v0.0.0-20180807145615-6ff867650f40
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/meatballhat/negroni-logrus v1.1.0
	github.com/prometheus/client_golang v1.8.0
	github.com/prometheus/common v0.14.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/negroni v1.0.0
	google.golang.org/protobuf v1.25.0 // indirect
)
