package main

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/conf"
	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/handlers"
	"gitlab.com/koalalorenzo/visma-challenge/mime-proxy/monitoring"
)

func main() {
	// Parsing the configuration from env variables
	c, err := conf.ParseConfiguration()
	if err != nil {
		c.Usage()
		return
	}

	// Create and set up logging
	log := logrus.New()

	switch c.Log.Format {
	case "json":
		log.SetFormatter(&logrus.JSONFormatter{})
	default:
		log.SetFormatter(&logrus.TextFormatter{})
	}

	switch c.Log.Format {
	case "debug":
		log.SetLevel(logrus.DebugLevel)
	case "warn":
		log.SetLevel(logrus.WarnLevel)
	default:
		log.SetLevel(logrus.TraceLevel)
	}

	go func() {
		err = monitoring.SetupHealthChecks(c)
		if err != nil {
			log.Fatal(err)
		}
	}()

	go func() {
		err = monitoring.SetupPrometheusEndpoint(c)
		if err != nil {
			log.Fatal(err)
		}
	}()

	err = handlers.Listen(c, log)
	if err != nil {
		log.Error(err)
	}
}
